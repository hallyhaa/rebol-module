@TemplateRegistration(
        folder = "Other",
        iconBase="org/babelserver/nb/rebolmodule/Rebol-icon.png", 
        displayName = "#HTMLtemplate_displayName", 
        content = "RebolScript.r",
        description = "Description.html",
        scriptEngine="freemarker")
@Messages(value = "HTMLtemplate_displayName=REBOL script file")
package org.babelserver.nb.rebolmodule.rebolFileTemplate;

import org.netbeans.api.templates.TemplateRegistration;
import org.openide.util.NbBundle.Messages;
