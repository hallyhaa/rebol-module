<#assign licensePrefix = "; ">
<#include "../Licenses/license-${project.license}.txt">

REBOL [ 
  Author: "${user}" 
  Title: "${name}" 
  File: %${nameAndExt}
  <#setting locale="en_UK"><#-- Need english names for months -->
  Date: ${dateTime?string("d-MMM-yyyy")}
  Version: 1
<#if project.displayName?? && project.displayName != "">
  Project: "${project.displayName}"
</#if>
  Purpose: {}
  Usage: {} 
]

speak: func [name] [
  ; simple hello world code
  print rejoin ["hello, " name]
]


<#-- 

TODO:
  https://platform.netbeans.org/tutorials/nbm-filetemplates.html#freemarker
  Det finnes et objekt av typen javax.swing.ScriptContext, som det kunne være morsomt å grave i...
  På lengre sikt kunne man ha en IF-basert oppføring av Library-blokk i headeren,
  og kanskje de to siste oppføringene, Purpose og Usage, også kunne være IF-et, eller
  det kunne være mulig å fylle dem ut i dialogruten man får på skjermen når man klikker
  på "ny rebol-fil"...

-->
