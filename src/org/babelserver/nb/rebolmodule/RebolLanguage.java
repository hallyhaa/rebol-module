package org.babelserver.nb.rebolmodule;

import org.netbeans.api.lexer.Language;
import org.netbeans.modules.csl.spi.DefaultLanguageConfig;
import org.netbeans.modules.csl.spi.LanguageRegistration;
import org.babelserver.nb.rebolmodule.lexer.RebolTokenId;
import org.babelserver.nb.rebolmodule.parser.MyRebolParser;
import org.babelserver.nb.rebolmodule.parser.codefolding.RebolStructureScanner;
import org.netbeans.modules.csl.api.StructureScanner;
import org.netbeans.modules.parsing.spi.Parser;

@LanguageRegistration(mimeType = "text/x-rebol")
public class RebolLanguage extends DefaultLanguageConfig {

  @Override
  public Language<RebolTokenId> getLexerLanguage() {
    return RebolTokenId.getLanguage();
  }

  @Override
  public String getDisplayName() {
    return "Rebol";
  }

  @Override
  public Parser getParser() {
    return new MyRebolParser();
  }

  @Override
  public StructureScanner getStructureScanner() {
    return new RebolStructureScanner();
  }

  @Override
  public boolean hasStructureScanner() {
    return true;
  }

}