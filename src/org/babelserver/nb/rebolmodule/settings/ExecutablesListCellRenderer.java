package org.babelserver.nb.rebolmodule.settings;

import java.awt.Component;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JList;

public class ExecutablesListCellRenderer extends DefaultListCellRenderer {

  /**
   * Returns a new instance of PropertyListCellRenderer. Nothing is changed here
   * from the super class. Icons and text colour are determined from the
   * UIManager.
   */
  public ExecutablesListCellRenderer() {
    super();
  }

  /**
   * Configures the renderer.
   *
   * @param list the JList to which we belong. This should always be a
   * PropertyList
   * @param value the value of this node.
   * @param index The cells index.
   * @param isSelected True if the specified cell was selected.
   * @param cellHasFocus True if the specified cell has the focus.
   * @return A component whose paint() method will render the specified value.
   */
  @Override
  public Component getListCellRendererComponent(
          JList list,
          Object value,
          int index,
          boolean isSelected,
          boolean cellHasFocus) {
    DefaultListCellRenderer toReturn = (DefaultListCellRenderer) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
    RebolExecutable rex = (RebolExecutable) value;
    if (rex.isStandardRunRebolAction()) {
      toReturn.setIcon(new ImageIcon(getClass().getResource("/org/babelserver/nb/rebolmodule/Rebol-icon16p.png"))); // NOI18N
      toReturn.setToolTipText("This is the standard rebol executable");
    } else {
      toReturn.setIcon(new ImageIcon(getClass().getResource("/org/babelserver/nb/rebolmodule/Transparent16p.png"))); // NOI18N
      toReturn.setToolTipText(null);
    }
    return toReturn;
  }
}