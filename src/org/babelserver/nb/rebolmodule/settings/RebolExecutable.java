package org.babelserver.nb.rebolmodule.settings;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import org.netbeans.api.extexecution.ExternalProcessBuilder;

/**
 * Represents a Rebol executable file
 */
public final class RebolExecutable implements Serializable {

  private String path;
  private String displayName;
  private boolean standardRunRebolAction;

  /* Nullary constructor for XML serialization purposes */
  public RebolExecutable() {
  }

  public RebolExecutable(String path) throws IOException, NullPointerException, NotRebolException {
    setPath(path);
    setDisplayName(displayNameForPath(path));
    setStandardRunRebolAction(false);
  }

  public boolean getTHEStandardRunRebolAction() {
    return true; //RebolPanel.
  }

  public boolean isStandardRunRebolAction() {
    return standardRunRebolAction;
  }

  public void setStandardRunRebolAction(boolean standardRunRebolAction) {
    this.standardRunRebolAction = standardRunRebolAction;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  @Override
  public String toString() {
    return getDisplayName();
  }

  @Override
  public boolean equals(Object obj) {
    String os = System.getProperty("os.name");
    if (os != null && os.toLowerCase().contains("windows")) {
      // windows platform fs is case insensitive for path names, so IgnoreCase here:
      return (obj instanceof RebolExecutable && ((RebolExecutable)obj).getPath().equalsIgnoreCase(path));
    }
    return (obj instanceof RebolExecutable && ((RebolExecutable)obj).getPath().equals(path));
  }

  private String displayNameForPath(String path) throws IOException, NullPointerException, NotRebolException {
    String product = getSystemInformation("product", path).toLowerCase();
    if (!(product.startsWith("core") || product.startsWith("view") || product.startsWith("rebol 3"))) {
      throw new NotRebolException(String.format("Given file doesn't seem to be a rebol executable: %s", path));
    }
    if (product.startsWith("rebol 3")) {
      product = "R3";
    }
    String version = getSystemInformation("version", path).toLowerCase();
    if (version.startsWith("rebol 3")) {
      version = version.substring(8); // removes "REBOL 3 "
    }
    return String.format("%s/%s", product, version);
  }

  private String getSystemInformation(String information, String path) throws IOException {
    ExternalProcessBuilder processBuilder = new ExternalProcessBuilder(path).addArgument("-cqvw").addArgument("--do").addArgument(String.format("print system/%s", information));
    Process p = processBuilder.call();
    BufferedInputStream buffer = new BufferedInputStream(p.getInputStream());
    BufferedReader result = new BufferedReader(new InputStreamReader(buffer));
    // we're only supposed to get one line of response, so here goes:
    String s = result.readLine();
    // instead of:
//    String s = "", line = "";
//    while ((line = result.readLine()) != null) {
//      s += line;
//    }

    // then clean up:
    result.close();
    buffer.close();
    p.destroy();

    return s;
  }
}
