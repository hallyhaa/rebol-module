package org.babelserver.nb.rebolmodule.settings;

/**
 *
 * @author Hallvard
 */
public class NotRebolException extends Exception {

  public NotRebolException(String message) {
    super(message);
  }
  
}
