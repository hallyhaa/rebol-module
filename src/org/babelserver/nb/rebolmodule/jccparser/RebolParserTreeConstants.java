/* Generated By:JavaCC: Do not edit this line. RebolParserTreeConstants.java Version 6.1_0 */
package org.babelserver.nb.rebolmodule.jccparser;

public interface RebolParserTreeConstants
{
  public int JJTROOT = 0;
  public int JJTREBOLHEADER = 1;
  public int JJTVOID = 2;
  public int JJTBLOCK = 3;
  public int JJTPARENS = 4;
  public int JJTCOMMENTS = 5;
  public int JJTSTRING = 6;
  public int JJTDATA = 7;
  public int JJTOP = 8;
  public int JJTWORD = 9;


  public String[] jjtNodeName = {
    "root",
    "rebolHeader",
    "void",
    "block",
    "parens",
    "comments",
    "string",
    "data",
    "op",
    "word",
  };
}
/* JavaCC - OriginalChecksum=a68642adb26f8f76176574e7b6cfb128 (do not edit this line) */
