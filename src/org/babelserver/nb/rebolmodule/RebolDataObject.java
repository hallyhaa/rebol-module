package org.babelserver.nb.rebolmodule;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import org.netbeans.core.spi.multiview.MultiViewElement;
import org.netbeans.core.spi.multiview.text.MultiViewEditorElement;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.MIMEResolver;
import org.openide.loaders.DataNode;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;

@MIMEResolver.ExtensionRegistration(
        displayName = "#LBL_Rebol_LOADER",
        mimeType = "text/x-rebol",
        showInFileChooser = {"Rebol files"},
        extension = {"r", "R", "r3", "R3", "reb"})
@DataObject.Registration(
        mimeType = "text/x-rebol",
        iconBase = "org/babelserver/nb/rebolmodule/Rebol-icon.png",
        displayName = "#LBL_Rebol_LOADER",
        position = 300)
@ActionReferences({
  @ActionReference(
          path = "Loaders/text/x-rebol/Actions",
          id =
          @ActionID(category = "System", id = "org.openide.actions.OpenAction"),
          position = 100,
          separatorAfter = 200),
  @ActionReference(
          path = "Loaders/text/x-rebol/Actions",
          id =
          @ActionID(category = "Edit", id = "org.openide.actions.CutAction"),
          position = 300),
  @ActionReference(
          path = "Loaders/text/x-rebol/Actions",
          id =
          @ActionID(category = "Edit", id = "org.openide.actions.CopyAction"),
          position = 400,
          separatorAfter = 500),
  @ActionReference(
          path = "Loaders/text/x-rebol/Actions",
          id =
          @ActionID(category = "Edit", id = "org.openide.actions.DeleteAction"),
          position = 600),
  @ActionReference(
          path = "Loaders/text/x-rebol/Actions",
          id =
          @ActionID(category = "System", id = "org.openide.actions.RenameAction"),
          position = 700,
          separatorAfter = 800),
  @ActionReference(
          path = "Loaders/text/x-rebol/Actions",
          id =
          @ActionID(category = "System", id = "org.openide.actions.SaveAsTemplateAction"),
          position = 900,
          separatorAfter = 1000),
  @ActionReference(
          path = "Loaders/text/x-rebol/Actions",
          id =
          @ActionID(category = "System", id = "org.openide.actions.FileSystemAction"),
          position = 1100,
          separatorAfter = 1200),
  @ActionReference(
          path = "Loaders/text/x-rebol/Actions",
          id =
          @ActionID(category = "System", id = "org.openide.actions.ToolsAction"),
          position = 1300),
  @ActionReference(
          path = "Loaders/text/x-rebol/Actions",
          id =
          @ActionID(category = "System", id = "org.openide.actions.PropertiesAction"),
          position = 1400)
})
public class RebolDataObject extends MultiDataObject {
  
  @Override
  public HelpCtx getHelpCtx() {
    return super.getHelpCtx();
  }

  public RebolDataObject(FileObject pf, MultiFileLoader loader) throws DataObjectExistsException, IOException {
    super(pf, loader);
    //registerEditor("text/x-rebol", true); // true = multiView. Maybe some other time.
    registerEditor("text/x-rebol", false); 
  }

  @Override
  protected int associateLookup() {
    return 1;
  }

  @MultiViewElement.Registration(
          displayName = "#LBL_Rebol_EDITOR",
          iconBase = "org/babelserver/nb/rebolmodule/Rebol-icon.png",
          mimeType = "text/x-rebol",
          persistenceType = TopComponent.PERSISTENCE_ONLY_OPENED,
          preferredID = "Rebol",
          position = 1000)
  public static MultiViewEditorElement createEditor(Lookup lkp) {
    return new MultiViewEditorElement(lkp);
  }

  /**
   *
   * @return
   */
  @Override
  protected Node createNodeDelegate() {
    return new RebolNode(this,
                     Children.LEAF,
                     getLookup());
  }

  // I DENNE "INNER CLASS" REGULERER
  // VI BLANT ANNET HVILKE EGENSKAPER
  // SOM DUKKER OPP I EGENSKAPSVINDUER
  // SOM KNYTTES TIL REBOL-FILER
  class RebolNode extends DataNode {

    public RebolNode(RebolDataObject aThis, Children kids, Lookup lookup) {
      super(aThis, kids, lookup);
    }

    @Override
    protected Sheet createSheet() {
      Sheet sheet = super.createSheet();
      Sheet.Set set = Sheet.createPropertiesSet();
      sheet.put(set);
      set.put(new RebolNode.LineCountProperty(this));
      set.put(new RebolNode.AuthorProperty(this));
      return sheet;
    }

    // MEINE ERSTE VERSUCH, EINE PROPERTY ZU MACHEN
    // Denne kan knyttes til rebol-headeren siden...
    private class AuthorProperty extends PropertySupport.ReadWrite<String> {

      private final RebolNode node;

      public AuthorProperty(RebolNode node) {
        super("author", String.class, "Author", "Author Name");
        this.node = node;
      }

      @Override
      public String getValue() throws IllegalAccessException, InvocationTargetException {
        return "E. X. Ample";
      }

      @Override
      public void setValue(String newAuthor) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
//        String msg = "Aha, \"" + newAuthor + "\" Skal bli det nye navnet?";
//        NotifyDescriptor nd = new NotifyDescriptor.Message(msg);
//        DialogDisplayer.getDefault().notify(nd);
      }
    }

    private class LineCountProperty extends PropertySupport.ReadOnly<Integer> {

      private final RebolNode node;

      public LineCountProperty(RebolNode node) {
        super("lineCount", Integer.class, "Line Count", "Number of Lines");
        this.node = node;
      }

      @Override
      public Integer getValue() throws IllegalAccessException, InvocationTargetException {
        int lineCount = 0;
        DataObject rDobj = node.getDataObject();
        FileObject rFo = rDobj.getPrimaryFile();
        try {
          lineCount = rFo.asLines().size();
        } catch (IOException ex) {
          Exceptions.printStackTrace(ex);
        }
        return lineCount;
      }
    }

  }

}
