package org.babelserver.nb.rebolmodule.parser.errorhighlighting;

import java.util.ArrayList;
import java.util.List;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.StyledDocument;
import org.babelserver.nb.rebolmodule.jcclexer.RebolParserConstants;
import org.netbeans.modules.parsing.spi.Parser.Result;
import org.netbeans.modules.parsing.spi.ParserResultTask;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.spi.editor.hints.ErrorDescription;
import org.netbeans.spi.editor.hints.ErrorDescriptionFactory;
import org.netbeans.spi.editor.hints.HintsController;
import org.netbeans.spi.editor.hints.Severity;
import org.openide.text.NbDocument;
import org.openide.util.Exceptions;
import org.babelserver.nb.rebolmodule.jccparser.ParseException;
import org.babelserver.nb.rebolmodule.jccparser.Token;
import org.babelserver.nb.rebolmodule.parser.MyRebolParser.MyRebolParserResult;
import org.netbeans.modules.parsing.spi.Parser;

public class RebolSyntaxErrorHighlightingTask<T extends Parser.Result> extends ParserResultTask<T> {

  @Override
  public void run(Result result, SchedulerEvent event) {
    try {
      MyRebolParserResult rebolResult = (MyRebolParserResult) result;
      List<ParseException> syntaxErrors = rebolResult.getRebolParser().syntaxErrors;
      Document document = rebolResult.getSnapshot().getSource().getDocument(false);
      List<ErrorDescription> errors = new ArrayList<>();
      for (ParseException syntaxError : syntaxErrors) {
        Token token = syntaxError.currentToken.next;
        int start = NbDocument.findLineOffset((StyledDocument) document, token.beginLine - 1) + token.beginColumn - 1;
        int end = NbDocument.findLineOffset((StyledDocument) document, token.endLine - 1) + token.endColumn;
        //System.out.print("start er " + start);
        //System.out.println(" og end er " + end);
        //Token wrongToken = syntaxError.currentToken.next;
        //String message = wrongToken.image + " isn't possible here!\n";
        ErrorDescription errorDescription = ErrorDescriptionFactory.createErrorDescription(
                                                                                            Severity.ERROR,
                                                                                            syntaxError.getMessage(), // message
                                                                                            document,
                                                                                            document.createPosition(start),
                                                                                            document.createPosition(end)
                                                                                          );
        errors.add(errorDescription);
      }
      HintsController.setErrors(document, "rebol", errors);
    } catch (BadLocationException ex1) {
      Exceptions.printStackTrace(ex1);
    } catch (org.netbeans.modules.parsing.spi.ParseException ex1) {
      Exceptions.printStackTrace(ex1);
    }
  }

  @Override
  public int getPriority() {
    return 100;
  }

  @Override
  public Class<? extends Scheduler> getSchedulerClass() {
    return Scheduler.EDITOR_SENSITIVE_TASK_SCHEDULER;
  }

  @Override
  public void cancel() {
  }

}