package org.babelserver.nb.rebolmodule.parser.errorhighlighting;

/**
 *
 * @author Hallvard
 */
import java.util.Collection;
import java.util.Collections;
import org.babelserver.nb.rebolmodule.parser.MyRebolParser;
import org.netbeans.api.editor.mimelookup.MimeRegistration;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.spi.SchedulerTask;
import org.netbeans.modules.parsing.spi.TaskFactory;

@MimeRegistration(mimeType = "text/x-rebol", service = TaskFactory.class)
public class RebolSyntaxErrorHighlightingTaskFactory extends TaskFactory {

  @Override
  public Collection<? extends SchedulerTask> create(Snapshot snapshot) {
    //RebolSyntaxErrorHighlightingTask<MyRebolParser.MyRebolParserResult> task;
    //task = new RebolSyntaxErrorHighlightingTask<MyRebolParser.MyRebolParserResult>();
    //return Collections.singleton(task);
    return Collections.singleton(new RebolSyntaxErrorHighlightingTask<MyRebolParser.MyRebolParserResult>());
  }
}
