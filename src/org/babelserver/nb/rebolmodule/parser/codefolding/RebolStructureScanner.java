package org.babelserver.nb.rebolmodule.parser.codefolding;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.StyledDocument;
import org.babelserver.nb.rebolmodule.jccparser.SimpleNode;
import org.babelserver.nb.rebolmodule.jccparser.Token;
import org.babelserver.nb.rebolmodule.parser.MyRebolParser;
import org.netbeans.api.editor.fold.FoldType;
import org.netbeans.api.editor.fold.FoldUtilities;
import org.netbeans.modules.csl.api.OffsetRange;
import org.netbeans.modules.csl.api.StructureItem;
import org.netbeans.modules.csl.api.StructureScanner;
import org.netbeans.modules.csl.spi.ParserResult;
import org.openide.text.NbDocument;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

/**
 *
 * @author Hallvard
 */
public class RebolStructureScanner implements StructureScanner {

  public static final String REBOL_BLOCK_FOLDS = "rebolblocks"; // doesn't work
  public static final String CODE_FOLDS = "codeblocks"; // works, but java style with curly brackets
  public static final String IMPORT_FOLDS = "imports"; // yields three dots (so usable)
  public static final String OTHER_CODE_FOLDS = "othercodeblocks"; // java style with curly brackets
  public static final String COMMENT_FOLDS = "comments"; // ?
  //private static final Logger logger = Logger.getLogger(RebolStructureScanner.class.getName());

  @Override
  public List<? extends StructureItem> scan(ParserResult pr) {
    //logger.log(Level.INFO, "Her er vi i SCAN! Vi mottar {0}", pr);
    return Collections.emptyList();
  }

  @Override
  public Map<String, List<OffsetRange>> folds(ParserResult pr) {
    //logger.log(Level.ALL, "RebolStructureScanner.folds, receiving {0}", pr);
    //new Error("Hvor kommer vi fra?").printStackTrace();
    HashMap<String, List<OffsetRange>> folds = new HashMap<String, List<OffsetRange>>();
    if (pr == null) {
      // you never know
      return folds;
    }

    MyRebolParser.MyRebolParserResult res = (MyRebolParser.MyRebolParserResult) pr;
    StyledDocument document = (StyledDocument) pr.getSnapshot().getSource().getDocument(false);

    try {
      SimpleNode root = (SimpleNode) res.getRebolParser().getAST().rootNode();
      // TODO: recursively traverse AST to find blocks that are not top level blocks.
      //root.dump("Dump:");
      makeBlockFolds(root, folds, document);
    } catch (Exception e) {
      Exceptions.printStackTrace(e);
    }
    //logger.log(Level.ALL, "StructureScanner: Sender fra oss FOLDS: {0}", folds);
    return folds;
  }

  @Override
  public Configuration getConfiguration() {
    return null;
  }

  private void makeBlockFolds(SimpleNode oNode, HashMap<String, List<OffsetRange>> folds, StyledDocument document) {
    for (int i = 0; i < oNode.jjtGetNumChildren(); i++) {
      String type = "";
      SimpleNode node = (SimpleNode) oNode.jjtGetChild(i);
      // recursive call:
      makeBlockFolds(node, folds, document);
      Token jjtGetFirstToken = node.jjtGetFirstToken();
      Token jjtGetLastToken = node.jjtGetLastToken();
      if (node.toString().equals("rebolHeader")) {
        // no check for number of lines, like in the block section?
        type = OTHER_CODE_FOLDS;
      }
      if (node.toString().equals("string")) {
        if (jjtGetLastToken.endLine - jjtGetFirstToken.beginLine < 4) {
          // don't collapse short strings. Is FOUR a reasonable number of lines?
          // TODO: This could be a SETTING in a later version
          continue;
        }
        type = COMMENT_FOLDS;
      }
      if (node.toString().equalsIgnoreCase("block")) {
        if (jjtGetLastToken.endLine - jjtGetFirstToken.beginLine < 4) {
          // don't collapse short blocks. Is FOUR a reasonable number of lines?
          // TODO: This could be a SETTING in a later version
          continue;
        }
        type = IMPORT_FOLDS;
        //String type = REBOL_BLOCK_FOLDS;
      }

      if (!type.isEmpty()) {
        int start = NbDocument.findLineOffset(document, jjtGetFirstToken.beginLine - 1) + jjtGetFirstToken.beginColumn - 1;
        int end = NbDocument.findLineOffset(document, jjtGetLastToken.endLine - 1) + jjtGetLastToken.endColumn;
        OffsetRange range = new OffsetRange(start, end);
        List<OffsetRange> fold = folds.get(type);
        if (fold == null) {
          fold = new ArrayList<OffsetRange>();
        }
        fold.add(range);
        folds.put(type, fold);
      }
    }
  }
}
/*
            addFoldsOfType(scanner, "codeblocks", folds, result, doc, 
                    CODE_FOLDING_COLLAPSE_METHOD, CODE_BLOCK_FOLD_TEMPLATE);
            addFoldsOfType(scanner, "comments", folds, result, doc, 
                    CODE_FOLDING_COLLAPSE_JAVADOC, JAVADOC_FOLD_TEMPLATE);
            addFoldsOfType(scanner, "initial-comment", folds, result, doc, 
                    CODE_FOLDING_COLLAPSE_INITIAL_COMMENT, INITIAL_COMMENT_FOLD_TEMPLATE);
            addFoldsOfType(scanner, "imports", folds, result, doc, 
                    CODE_FOLDING_COLLAPSE_IMPORT, IMPORTS_FOLD_TEMPLATE);
            addFoldsOfType(scanner, "tags", folds, result, doc, 
                    CODE_FOLDING_COLLAPSE_TAGS, TAG_FOLD_TEMPLATE);
            addFoldsOfType(scanner, "othercodeblocks", folds, result, doc, 
                    CODE_FOLDING_COLLAPSE_TAGS, CODE_BLOCK_FOLD_TEMPLATE);
            addFoldsOfType(scanner, "inner-classes", folds, result, doc, 
                    CODE_FOLDING_COLLAPSE_INNERCLASS, INNER_CLASS_FOLD_TEMPLATE);
*/