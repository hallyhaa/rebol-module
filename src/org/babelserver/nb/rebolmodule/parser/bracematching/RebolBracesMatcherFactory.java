package org.babelserver.nb.rebolmodule.parser.bracematching;

import org.netbeans.api.editor.mimelookup.MimeRegistration;
import org.netbeans.spi.editor.bracesmatching.BracesMatcher;
import org.netbeans.spi.editor.bracesmatching.BracesMatcherFactory;
import org.netbeans.spi.editor.bracesmatching.MatcherContext;
import org.netbeans.spi.editor.bracesmatching.support.BracesMatcherSupport;

@MimeRegistration(mimeType = "text/x-rebol", service = BracesMatcherFactory.class, position = 50)
public class RebolBracesMatcherFactory implements BracesMatcherFactory {

  // since strings are recognized and colourized as tokens, 
  // we don't need '{' and '}' (from the default implementation)
  static final char[] brackets = new char[]{'[', ']', '(', ')', '<', '>'};

  @Override
  public BracesMatcher createMatcher(MatcherContext context) {
    //return BracesMatcherSupport.defaultMatcher(context, -1, -1);
    return BracesMatcherSupport.characterMatcher(context, -1, -1, brackets);
  }
}