package org.babelserver.nb.rebolmodule.parser.indentation;

import org.netbeans.api.editor.mimelookup.MimeRegistration;
import org.netbeans.modules.editor.indent.spi.Context;
import org.netbeans.modules.editor.indent.spi.IndentTask;

@MimeRegistration(mimeType = "text/x-rebol", service = IndentTask.Factory.class)
public class RebolIndentTaskFactory implements IndentTask.Factory {

  @Override
  public IndentTask createTask(Context context) {
    return new RebolIndentTask(context);
  }
}
