package org.babelserver.nb.rebolmodule.parser.indentation;

import javax.swing.text.BadLocationException;
import org.netbeans.modules.editor.indent.spi.Context;
import org.netbeans.modules.editor.indent.spi.ExtraLock;
import org.netbeans.modules.editor.indent.spi.IndentTask;

/**
 * Dependency: Editor Indentation
 */
public class RebolIndentTask implements IndentTask {

  private Context context;
  private final int indentSize = 2; // should be a setting somewhere?

  RebolIndentTask(Context context) {
    this.context = context;
  }

  @Override
  public void reindent() throws BadLocationException {
    if (context == null) {
      // you never know!
      return;
    }

    int offset = context.lineStartOffset(context.caretOffset());
    int prevOffset = context.lineStartOffset(offset - 1);
    int prevIndent = context.lineIndent(prevOffset);
    String prevLine = context.document().getText(prevOffset, offset - prevOffset);
    int newIndent = Math.max(0, prevIndent + getIndent(prevLine));
    context.modifyIndent(offset, newIndent);
  }

  private int getIndent(String previousLine) {
    int indentChange = 0;
    for (int pos = 0; pos < (previousLine.length() - 1); pos++) {
      switch (previousLine.charAt(pos)) {
        case '[':
        case '(':
          indentChange++;
          break;
        case ']':
        case ')':
          indentChange--;
          break;
      }
    }
    return indentChange * indentSize;
  }

  @Override
  public ExtraLock indentLock() {
    return null;
  }
}
