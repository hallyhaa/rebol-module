package org.babelserver.nb.rebolmodule.parser.formatting;

import org.netbeans.api.editor.mimelookup.MimeRegistration;
import org.netbeans.modules.editor.indent.spi.Context;
import org.netbeans.modules.editor.indent.spi.ReformatTask;

/**
 *
 * @author Hallvard
 */
@MimeRegistration(mimeType = "text/x-rebol", service = ReformatTask.Factory.class)
public class RebolReformatTaskFactory implements ReformatTask.Factory {

  @Override
  public ReformatTask createTask(Context context) {
    return new RebolReformatTask(context);
  }
}
