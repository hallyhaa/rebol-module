package org.babelserver.nb.rebolmodule.parser.formatting;

import javax.swing.text.BadLocationException;
import org.netbeans.modules.editor.indent.spi.Context;
import org.netbeans.modules.editor.indent.spi.ExtraLock;
import org.netbeans.modules.editor.indent.spi.ReformatTask;

// TODO: det er mer enn bare indentation som skal ordnes med reformat-metoden.
/**
 *
 * @author Hallvard
 */
public class RebolReformatTask implements ReformatTask {

  private final Context context;
  private final int indentSize = 2; // should be a setting somewhere?

  public RebolReformatTask(Context context) {
    this.context = context;
  }

  @Override
  public void reformat() throws BadLocationException {
    if (context == null) {
      // you never know!
      return;
    }

    boolean inString = false;
    int openBrackets = 0;
    int openParens = 0;
    int currentIndent = 0;
    String source = context.document().getText(0, context.document().getLength());

    for (int pos = 0; pos < source.length(); pos++) {
      if (inString) {
        switch (source.charAt(pos)) {
          case '}': // TODO: nested strings...
            if (source.charAt(pos) - 1 != '^') {
              inString = false;
            }
            break;
        }
      } else {
        switch (source.charAt(pos)) {
          case '[':
            openBrackets++;
            currentIndent++;
            break;
          case '(':
            openParens++;
            currentIndent++;
            break;
          case ']':
            openBrackets--;
            currentIndent--;
            break;
          case ')':
            openParens--;
            currentIndent--;
            break;
          case '{':
            inString = true;
            break;
          case '"':
            for (int i = pos; i < source.length(); i++) {
              if ((source.charAt(pos) == '"') ||
                  (source.charAt(pos) == '\n')) {
                pos = i;
                break; // inner for loop
              }
              pos = i;
            }
            break; // switch
          case '\n':
            currentIndent = Math.max(0, currentIndent);
            context.modifyIndent(context.lineStartOffset(pos + 1), currentIndent * indentSize);
            break;
        } // end switch
      } // end not inString
    } // end for loop
  }

  @Override
  public ExtraLock reformatLock() {
    return null;
  }
}
