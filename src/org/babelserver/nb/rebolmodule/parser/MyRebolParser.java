package org.babelserver.nb.rebolmodule.parser;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeListener;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Task;
import org.netbeans.modules.parsing.spi.Parser;
//import org.netbeans.modules.parsing.spi.Parser.Result;
import org.netbeans.modules.parsing.spi.SourceModificationEvent;
import org.netbeans.modules.parsing.spi.ParseException;
import org.babelserver.nb.rebolmodule.jccparser.RebolParser;
import org.babelserver.nb.rebolmodule.jccparser.SimpleNode;
import org.netbeans.modules.csl.api.Error;
import org.netbeans.modules.csl.spi.ParserResult;

/**
 *
 * @author Hallvard
 */
public class MyRebolParser extends Parser {

  private Snapshot snapshot;
  private RebolParser jccRebolParser;

  public MyRebolParser() {
    super();
  }

  @Override
  public void parse(Snapshot snapshot, Task task, SourceModificationEvent event) {
    this.snapshot = snapshot;
    Reader reader = new StringReader(snapshot.getText().toString());
    jccRebolParser = new RebolParser(reader);
    try {
      jccRebolParser.CompilationUnit();
    } catch (Exception ex) {
      Logger.getLogger(MyRebolParser.class.getName()).log(Level.WARNING, null, ex);
    }
  }

  @Override
  public Result getResult(Task task) {
    return new MyRebolParserResult(snapshot, jccRebolParser);
  }

  @Override
  @Deprecated
  public void cancel() {
  }

  @Override
  public void addChangeListener(ChangeListener changeListener) {
  }

  @Override
  public void removeChangeListener(ChangeListener changeListener) {
  }

  public static class MyRebolParserResult extends ParserResult {

    private RebolParser jccRebolParser;
    private boolean valid = true;

    MyRebolParserResult(Snapshot snapshot, RebolParser jccRebolParser) {
      super(snapshot);
      this.jccRebolParser = jccRebolParser;
    }

    public RebolParser getRebolParser() throws ParseException {
      if (!valid) {
        throw new ParseException();
      }
      return jccRebolParser;
    }

    @Override
    public Snapshot getSnapshot() {
      return super.getSnapshot();
    }

    @Override
    protected void invalidate() {
      valid = false;
    }

    @Override
    public List<? extends Error> getDiagnostics() {
      // TODO (hvordan brukes denne metoden egentlig?):
      return new ArrayList<Error>();
    }

  }

}
