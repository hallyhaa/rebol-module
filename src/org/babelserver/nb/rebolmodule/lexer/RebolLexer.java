package org.babelserver.nb.rebolmodule.lexer;

import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;
import org.babelserver.nb.rebolmodule.jcclexer.SimpleCharStream;
import org.babelserver.nb.rebolmodule.jcclexer.RebolParserTokenManager;
import org.babelserver.nb.rebolmodule.jcclexer.Token;
import org.babelserver.nb.rebolmodule.jcclexer.TokenMgrError;

class RebolLexer implements Lexer<RebolTokenId> {

  private LexerRestartInfo<RebolTokenId> info;
  private RebolParserTokenManager rebolParserTokenManager;
  private static SimpleCharStream stream = null;

  RebolLexer(LexerRestartInfo<RebolTokenId> info) {
    this.info = info;
    //if (stream == null) {
      stream = new SimpleCharStream(info.input());
      rebolParserTokenManager = new RebolParserTokenManager(stream);
    //} else {
    //  stream = new SimpleCharStream(info.input());
    //  RebolParserTokenManager.ReInit(stream);
    //}
  }

  @Override
  public org.netbeans.api.lexer.Token<RebolTokenId> nextToken() {
    // Utkommenterte linjer flytter en error fra lexer til parser...
    Token token=null;
    //try {
      token = rebolParserTokenManager.getNextToken();
    //} catch (TokenMgrError e) {
    //  System.out.println("HER ER VI OG TAR IMOT: " + e.getMessage());
    //  e.printStackTrace();
    //  return info.tokenFactory().createToken(RebolLanguageHierarchy.getToken(12)); // 12 er string
    //}
    //Token token = RebolParserTokenManager.getNextToken();
    if (info.input().readLength() < 1) {
      return null;
    }
//    if (token.kind != 1) {
//      System.out.print("Vi lager token: \"" + info.input().readText() + "\" ");
//    }
    return info.tokenFactory().createToken(RebolLanguageHierarchy.getToken(token.kind));
  }

  @Override
  public Object state() {
    return null;
  }

  @Override
  public void release() {
  }
}
