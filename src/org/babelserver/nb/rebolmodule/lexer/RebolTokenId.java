package org.babelserver.nb.rebolmodule.lexer;

import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.TokenId;

public class RebolTokenId implements TokenId {

  private final String name;
  private final String primaryCategory;
  private final int id;

  public RebolTokenId(
          String name,
          String primaryCategory,
          int id) {
    this.name = name;
    this.primaryCategory = primaryCategory;
    this.id = id;
  }

  @Override
  public String primaryCategory() {
    return primaryCategory;
  }

  @Override
  public int ordinal() {
    return id;
  }

  @Override
  public String name() {
    return name;
  }

  public static Language<RebolTokenId> getLanguage() {
    return new RebolLanguageHierarchy().language();
  }
}