package org.babelserver.nb.rebolmodule.lexer;

import java.io.IOException;
import java.io.Reader;
import org.netbeans.spi.lexer.LexerInput;
import org.netbeans.spi.lexer.LexerRestartInfo;

/**
 *
 * @author yst0808
 */
public class LexerInputReader extends Reader {

  private final LexerRestartInfo<RebolTokenId> LRI;

  public LexerInputReader(LexerRestartInfo<RebolTokenId> info) {
    super();
    this.LRI = info;
    //LexerInput input = LRI.input();
  }

  @Override
  public int read(char cbuf[], int off, int len) throws IOException {
    // as in java.io.BufferedReader:
    if ((off < 0) || (off > cbuf.length) || (len < 0)
            || ((off + len) > cbuf.length) || ((off + len) < 0)) {
      throw new IndexOutOfBoundsException();
    } else if (len == 0) {
      return 0;
    }
    
    // ... but then:
    int counter = off;
    int i = 0;
    while (counter < len && (i = LRI.input().read()) != LexerInput.EOF) {
      cbuf[counter] = (char) i;
      counter++;
    }
    return counter;
  }

  String GetImage() {
    return LRI.input().readText().toString();
  }
  
  @Override
  public void close() throws IOException {
    // Do we need to do anything here?
  }

  public LexerRestartInfo<RebolTokenId> getLexerRestartInfo() {
    return LRI;
  }
}
