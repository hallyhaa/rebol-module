package org.babelserver.nb.rebolmodule.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.babelserver.nb.rebolmodule.settings.RebolExecutable;
import org.babelserver.nb.rebolmodule.settings.RebolPanel;
import org.netbeans.api.extexecution.ExecutionDescriptor;
import org.netbeans.api.extexecution.ExecutionService;
import org.netbeans.api.extexecution.ExternalProcessBuilder;
import org.openide.DialogDisplayer;
import org.openide.LifecycleManager;
import org.openide.NotifyDescriptor;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.filesystems.FileObject;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.TopComponent;

@ActionID(
        category = "Build",
        id = "org.babelserver.nb.rebolmodule.RunRebolAction")
@ActionRegistration(
        iconBase = "org/babelserver/nb/rebolmodule/Rebol-icon16p.png",
        displayName = "#CTL_RunRebolAction")
@ActionReference(path = "Toolbars/Build", position = 500)
@Messages("CTL_RunRebolAction=Run in Rebol")
public final class RunRebolAction implements ActionListener {

  @Override
  public void actionPerformed(ActionEvent e) {
    final TopComponent tc = TopComponent.getRegistry().getActivated();
    final FileObject fo = tc.getLookup().lookup(FileObject.class);
    RebolExecutable rex = RebolPanel.getStandardRunRebolAction();
    if (rex == null || rex.getPath() == null || rex.getPath().isEmpty()) {
      NotifyDescriptor nd = new NotifyDescriptor.Message("No rebol executable is defined. "
                                                         + "Please open Tools | Options | Misscellaneous and define an executable in the "
                                                         + "rebol panel.", NotifyDescriptor.ERROR_MESSAGE);
      nd.setTitle("No rebol executable defined");
      DialogDisplayer.getDefault().notify(nd);
      return;
    }
    if (fo == null || !fo.getMIMEType().equals("text/x-rebol")) {

      NotifyDescriptor nd = new NotifyDescriptor.Message("No rebol document is in focus. "
                                                         + "Please open a rebol document.", NotifyDescriptor.ERROR_MESSAGE);
      nd.setTitle("Not a rebol script file");
      DialogDisplayer.getDefault().notify(nd);
      return;
    }
    LifecycleManager.getDefault().saveAll();
    String filename = fo.getPath();
    ExecutionDescriptor descriptor = new ExecutionDescriptor().controllable(true).frontWindow(true);
    ExternalProcessBuilder processBuilder = new ExternalProcessBuilder(rex.getPath())
            .addArgument("-c")
            .addArgument(filename);
    final ExecutionService exeService = ExecutionService.newService(processBuilder, descriptor, filename);
    /*Future<Integer> exitCode =*/ exeService.run();

    // TODO: if no output from rebol script (e.g. with a graphical thing or just
    // a library script run to check for rebol interpreter errors), NB will
    // keep title of output window in bold font, as if process not finished...

    // Can't wait() for the process - NB reacts as if something is wrong
  }
}
